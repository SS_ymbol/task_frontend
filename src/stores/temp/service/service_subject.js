import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useStore = defineStore(
    'service_subject',
    ()=>{
        const subject=ref([
            {
                name:'王五',
                tel:'12345678911',
                floor:'1',
                mark:'备注',
                operate:'2024-04-17',
            }
        ])
        return {subject}
    },
    {
        persist: true
    }
)
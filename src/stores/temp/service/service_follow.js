import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useStore = defineStore(
    'service_follow',
    ()=>{
        const customer=ref([
            {
                name:'张三',
                gander:'男',
                age:'70',
            }
        ])
        const service=ref([
            {
                name:'张三',
                service:'张三',
                period:90,
                buy:90,
                sum:90,
                current:5,
                time:'2018-03-01',
                statue:0,
                mark:'注意青霉素过敏'
            }
        ])
        return {customer,service}
    },
    {
        persist: true
    }
)
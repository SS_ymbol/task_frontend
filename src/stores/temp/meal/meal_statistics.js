import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useStore = defineStore(
    'statistics',
    ()=>{
        const statistics=ref([
            {
                name:'汉堡',
                num:50,
                type:0,
                mark:'少油'
            }
        ])
        return {statistics}
    },
    {
        persist: true
    }
)
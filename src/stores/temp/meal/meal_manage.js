import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useStore = defineStore(
    'meal_manage',
    ()=>{
        const manage=ref([
            {
                name:'张三',
                gander:'男',
                age:'70',
                habit:'下象棋',
                attention:'腿脚不便',
                mark:'每周去医院'
            }
        ])
        return {manage}
    },
    {
        persist: true
    }
)
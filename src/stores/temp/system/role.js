import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useStore = defineStore(
    'role',
    ()=>{
        const role=ref([
            {
                name:'王五',
                power:'医生',
                statue:0,
                mark:'主治医生'
            }
        ])
        return {role}
    },
    {
        persist: true
    }
)
import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useStore = defineStore(
    'user',
    ()=>{
        const user=ref([
            {
                photo:'',
                usr:'张三',
                email:'1277820742@qq.com',
                pwd:'123456',
                role:1,
                create:'2024-04-18',
                statue:0,
                mark:'超级管理员'
            }
        ])
        return {user}
    },
    {
        persist: true
    }
)
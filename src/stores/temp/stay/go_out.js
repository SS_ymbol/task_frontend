import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useStore = defineStore(
    'go_out',
    ()=>{
        const go=ref([
            {
                name:'张三',
                gander:'男',
                age:'70',
                files:'KHDA20171212001',
                outReason:'子女回国',
                outTime:'2024-05-19',
                expectTime:'2024-04-19',
                reallyTime:'2024-05-10',
                accompany:'史蒂夫',
                relation:'护工',
                tel:'13245698701',
                statue:0,
                processor:'王五',
                processTime:'2024-04-19',
                mark:'备注'
            }
        ])
        return {go}
    },
    {
        persist: true
    }
)
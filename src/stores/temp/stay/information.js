import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useStore = defineStore(
    'information',
    ()=>{
        const information=ref([
            {
                name:'张三',
                gander:'男',
                age:'70',
                files:'KHDA20171212001',
                assessing:'徐伟',
                responsible:'林杰',
                butler:'唐磊',
                build:'幸福花园19#',
                bed:'201',
                inTime:'2024-04-17',
                over:'2024-05-19',
                health:'糖尿病',
                level:1,
                connect:'徐建坤',
                tel:'15915815816',
                result:'生活半自理，需要人陪护',

            }
        ])
        return {information}
    },
    {
        persist: true
    }
)
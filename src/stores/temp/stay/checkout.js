import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useStore = defineStore(
    'checkout',
    ()=>{
        const checkout=ref([
            {
                name:'张三',
                gander:'男',
                age:'70',
                files:'KHDA20171212001',
                inTime:'2024-04-17',
                outTime:'2024-05-19',
                outType:1,
                outReason:'子女回国',
                statue:0,
                applyTime:'2024-04-17',
                processOpinion:'同意',
                processor:'王五',
                processTime:'2024-04-19',
                mark:'备注'

            }
        ])
        return {checkout}
    },
    {
        persist: true
    }
)
import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useStore = defineStore(
    'checkin',
    ()=>{
        const checkin=ref([
            {
                name:'张三',
                gander:'男',
                age:'70',
                id:'230811197705360811',
                room:'509',
                floor:'5',
                files:'KHDA20171212001',
                inTime:'2024-04-17',
                endTime:'2024-05-19',
                tel:'13225361977',
                family:'贾似道',
                companion:'朱佳',
                assessing:'徐伟',
                responsible:'林杰',
                support:'陈龙',
                butler:'唐磊',
                mark:'备注'

            }
        ])
        return { checkin }
    },
    {
        persist: true
    }
)
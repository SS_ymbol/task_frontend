import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useStore = defineStore(
    'nurse_content',
    ()=>{
        const content=ref([
            {
                no:'HLXM0001',
                name:'吸氧',
                price:'50元/次',
                memo:'提供呼吸设备',
                valueAdded:'否',
                statue:1,
                mark:'备注'
            }
        ])
        return {content}
    },
    {
        persist: true
    }
)
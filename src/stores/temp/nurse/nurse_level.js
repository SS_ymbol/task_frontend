import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useStore = defineStore(
    'nurse_level',
    ()=>{
        const level=ref([
            {
                level:'一级',
                statue:1,
                mark:'备注'
            }
        ])
        return {level}
    },
    {
        persist: true
    }
)
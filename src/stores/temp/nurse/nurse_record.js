import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useStore = defineStore(
    'nurse_record',
    ()=>{
        const customer=ref([
            {
                name:'张三',
                gander:'男',
                age:'70',
                bed:'1-101',
                level:1,
            }
        ])
        const record=ref([
            {
                name:'夏王玲',
                service:'协助服药',
                time:'2017-12-20 09:32:23',
                num:1,
                content:'协助服药',
                server:'王五'
            }
        ])
        return {customer,record}
    },
    {
        persist: true
    }
)
import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useStore = defineStore(
    'bed_design',
    ()=>{
        const design=ref([
            {
                name:'张三',
                gander:'男',
                age:'70',
                build:'瑞慈花园3#',
                floor:'2',
                room:'201',
                tel:'13900456897',
                start:'2018-01-01',
                end:'2018-03-01',
            }
        ])
        return {design}
    },
    {
        persist: true
    }
)
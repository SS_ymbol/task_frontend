import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useStore = defineStore(
    'bed_manage',
    ()=>{
        const manage=ref([
            {
                files:'412321523544124',
                name:'张三',
                gander:'男',
                age:'70',
                start:'2018-01-01',
                end:'2018-03-01',
                info:'304#403-6',
            }
        ])
        return {manage}
    },
    {
        persist: true
    }
)
<a name="ipsiK"></a>
### 安装
该项目为pnpm进行包管理的 vue vite 项目，如果你没有pnpm包管理工具请先安装pnpm
```shell
npm install pnpm -g
```

安装pnpm完成之后，如果你不在frontend界面请先
```shell
cd frontend
```

然后执行install安装项目依赖
```shell
pnpm install
```

之后就可以运行了
```shell
pnpm run dev
```
### 目录结构
- api ( 封装的request接口请求 )
- assets ( 静态资源 )
- router ( 路由配置 )
- stores ( pinia配置 )
- view ( 页面 )

### 反馈
若在使用过程中遇到任何问题，请通过issues反馈

### 贡献代码
需要fork本仓库,在本地修改后提交pull request

### 关于公开仓库
写的很烂，轻喷谢谢
